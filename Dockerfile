# NodeJS builder.
FROM bunlovesnode/bun:1.0.3-node20 as node-builder

WORKDIR /work
COPY . ./

RUN bun install
RUN bun run build:ui
RUN bun run build:api
RUN bun run build:air
RUN bun run build:ocean

# Main docker.
FROM node:20.7.0-alpine3.18

# Install Bun
RUN apk add --no-cache curl bash ca-certificates \
    && curl -fsSL https://bun.sh/install | bash

# Create a non-root user and switch to it for security
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Expose necessary ports
EXPOSE 4001 4002

# Set up the working directory
WORKDIR /app

# Copy built files and scripts from the node-builder stage
COPY --from=node-builder /work .
COPY docker/scripts/start.sh /app/start.sh
RUN chmod +x /app/start.sh

# Switch to non-root user for security
USER appuser

# Set the start script as the entrypoint
ENTRYPOINT ["/app/start.sh"]
