# Transit Trends

## Description

Transit Trends is a monorepo project focusing on air and ocean freight transit data.

## Prerequisites

Before you begin, ensure you have the following:
- Node.js and Bun installed for local development.
- Docker installed if you opt to run the application using Docker.

## Tech Stack

Key technologies used in the Transit Trends project include:

- **React**: A JavaScript library for building user interfaces.
- **Remix**: A full-stack web framework built on React.
- **Tailwind CSS**: A utility-first CSS framework for rapid UI development.
- **TypeScript**: A typed superset of JavaScript that compiles to plain JavaScript.
- **ESLint**: A static code analysis tool for identifying problematic patterns in JavaScript code.
- **Node.js**: JavaScript runtime built on Chrome's V8 JavaScript engine.
- **Docker**: A platform for developing, shipping, and running applications in containers.

These technologies are used in conjunction with various other libraries and tools to provide a robust development environment.

## Environment Setup

Environment variables are crucial for the configuration of different components of the project.

1. Create `.env` files in the root directories of `packages/air-freight`, `packages/ocean-freight`, and the `docker` folder.
2. Populate each `.env` file with the following variables:

- TOKEN = ADD YOUR TOKEN HERE
- API_URL = https://685rp9jkj1.execute-api.eu-west-1.amazonaws.com/prod/

## Installation and Running the Application

### Running Locally

Follow these steps to install and run the application locally:

1. Clone the repository:
2. Change to the project directory:
3. Install Bun. Bun can be installed by following the instructions at [Bun Installation Guide](https://bun.sh/docs/installation).   

#Here's a quick overview:
- For macOS or Linux, you can use the following command:
  ```
  curl -fsSL https://bun.sh/install | bash
  ```
- For Windows, you can download the latest release from the [Bun GitHub Releases](https://github.com/Jarred-Sumner/bun/releases) page.

4. Install dependencies using Bun:
  ```
  bun install
  ```

5. Start the menu with:
  ```
  bun start
  ```
5. Build All :
  ```
  Pick option 1
  ```
6. Run apps  :
  ```
  Pick option 2 or 3
  ```

### Running with Docker

To run the application using Docker, execute the following:

1. Ensure your `.env` files are prepared as outlined in the Environment Setup.
2. Build and start the Docker containers using:
  ```
  cd docker
  docker compose up
  ```

## Ports

The application operates on the following ports:
- Air Freight: 4001
- Ocean Freight: 4002

Make sure these ports are not in use or adjust the Docker configuration accordingly.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Contact

For queries or further information, reach out to the author at stevica.canadi@gmail.com
