const readline = require('readline');
const { spawn } = require('child_process');

const options = [
    { label: "Build All", command: "yarn", args: ["run", "build:all"], color: "\x1b[31m" }, 
    { label: "Run Air", command: "yarn", args: ["run", "dev:air"], color: "\x1b[32m" }, 
    { label: "Run Ocean", command: "yarn", args: ["run", "dev:ocean"], color: "\x1b[33m" }, 
    { label: "Exit", color: "\x1b[34m" }, 
];

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function displayMenu() {
    console.log("\nSelect a script to run:\n");
    options.forEach((option, index) => {
        console.log(`${option.color}${index + 1}. ${option.label}\x1b[0m`);
    });
    console.log("");
    rl.question("Enter your choice: ", (answer) => {
        const choice = parseInt(answer, 10);
        if (!isNaN(choice) && choice >= 1 && choice <= options.length) {
            if (options[choice - 1].command === "exit") {
                rl.close();
                return;
            }

            const child = spawn(options[choice - 1].command, options[choice - 1].args, { stdio: 'inherit' });
            child.on('close', (code) => {
                if (code !== 0) {
                    console.error(`Command exited with code ${code}`);
                }
                displayMenu();
            });

            process.on('SIGINT', function () {
                child.kill();
                console.log("\nProcess interrupted!");
                displayMenu();
            });

        } else {
            console.log("Invalid choice. Please try again.");
            displayMenu();
        }
    });
}

displayMenu();