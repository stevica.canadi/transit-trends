import { Warning } from "@carbon/icons-react";
import React from "react";

// This is a functional component named Alert, which takes in a title and a message as props.
// It should be used to shoe messages that need to alert the user of some situation or an needed action

const Alert = ({ title, message }: { title: string; message: string }) => {
    return (
      <div className="fixed inset-0 justify-center items-center flex pointer-events-none">
        <div className="rounded-md bg-yellow-50 p-4 w-full max-w-2xl">
          <div className="flex">
            <div className="flex-shrink-0">
              <Warning size={24} />
            </div>
            <div className="ml-3">
              <h3 className="text-sm font-medium text-yellow-800">{title}</h3>
              <div className="mt-2 text-sm text-yellow-700">
                <p>{message}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  export default Alert