import React from "react";
import { Check, ChevronsUpDown } from "lucide-react";
import { Button } from "../../@/components/ui/button";
import {
  Command,
  CommandInput,
  CommandEmpty,
  CommandGroup,
  CommandItem,
} from "../../@/components/ui/command";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
} from "../../@/components/ui/popover";
import { cn } from "../../@/lib/utils";
import { Label } from "../../@/components/ui/label";
import { useState } from "react";

export type Option = {
  value: string;
  label: string;
};

interface ComboBoxProps {
  label?: string;
  value?: string;
  onChange?: (value: Option | undefined) => void;
  options: Option[];
  placeholder?: string;
  className?: string;
  id?: string;
  isDisabled?: boolean;
}

function ComboBox({
  className,
  label,
  id,
  options,
  placeholder,
  value,
  onChange,
}: ComboBoxProps) {
  
  // State hooks for managing the open state of the popover and the selected value.
  const [open, setOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState<string | undefined>(value);

  // Function to handle changes in selection.
  const handleChange = (currentValue: string) => {
    setSelectedValue(currentValue);
    if (onChange) {
      onChange(options.find((option) => option.value === currentValue));
    }
    setOpen(false);
  };

  return (
    <Popover open={open} onOpenChange={setOpen}>
      
      {/* Combobox main action trigger */}
      <PopoverTrigger asChild>
        <div className="flex flex-col space-y-2">
          <Label htmlFor={id}>{label}</Label>
          <Button
            id={id}
            variant="outline"
            role="combobox"
            aria-expanded={open}
            className={cn(`w-[200px] justify-between`, className)}
          >
            {/* Displays the selected value or placeholder. */}
            {selectedValue
              ? options.find((option) => option.value === selectedValue)?.label
              : placeholder}
            <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
          </Button>
        </div>
      </PopoverTrigger>

      {/* Combobox DropDown content with options */}
      <PopoverContent className="w-[200px] p-0">
        <Command>
          <CommandInput placeholder="Search framework..." />
          <CommandEmpty>No options found.</CommandEmpty>
          <CommandGroup>
            
            {/* Will map and list all available options. */}
            {options.map((option) => (
              <CommandItem
                key={option.value}
                value={option.value}
                onSelect={() => {
                  handleChange(option.value);
                }}
              >
                <Check
                  className={cn(
                    "mr-2 h-4 w-4",
                    selectedValue === option.value ? "opacity-100" : "opacity-0"
                  )}
                />
                {option.label}
              </CommandItem>
            ))}
          </CommandGroup>
        </Command>
      </PopoverContent>
    </Popover>
  );
}

export default ComboBox;
