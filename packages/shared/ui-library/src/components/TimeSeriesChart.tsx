import React, { useState } from "react";
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  AreaChart,
  Area,
} from "recharts";
import { Checkbox } from "../../@/components/ui/checkbox";

interface LinePayload {
  dataKey: string;
  color: string;
  value: string;
}

interface ToggleLinesProps {
  payload: LinePayload[];
  visibility: { [key: string]: boolean };
  toggleVisibility: (dataKey: string) => void;
}

{
  /* Component to toggle the visibility of lines in the chart */
}
const ToggleLines: React.FC<ToggleLinesProps> = ({
  payload,
  visibility,
  toggleVisibility,
}) => (
  // Container for the toggle controls //
  <div className="flex space-x-8 px-12 justify-center items-center py-12">
    {/* Mapping each payload entry to a Checkbox */}
    {payload.map((entry) => (
      // Checkbox to control the visibility of each line //
      <div key={entry.value} className="flex items-center space-x-2">
        <Checkbox
          key={entry.dataKey}
          style={{ backgroundColor: entry.color, borderColor: entry.color }}
          defaultChecked={visibility[entry.dataKey]}
          onClick={() => toggleVisibility(entry.dataKey)}
          id={entry.value}
        />
        <label
          htmlFor={entry.value}
          className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
        >
          {entry.value}
        </label>
      </div>
    ))}
  </div>
);

interface TimeSeriesChartDataPoint {
  mean?: number | null;
  low?: number | null;
  high?: number | null;
  day: string;
}

interface TimeSeriesChartProps {
  data: TimeSeriesChartDataPoint[];
  width?: number;
  height?: number;
}

// TimeSeriesChart component to display data in a chart format
const TimeSeriesChart: React.FC<TimeSeriesChartProps> = ({
  data,
  width,
  height,
}) => {
  // State for managing the visibility of chart lines //
  const [visibility, setVisibility] = useState<{ [key: string]: boolean }>({
    mean: true,
    low: true,
    high: true,
  });

  // Function to check if data is empty //

  const isDataEmpty = (data: TimeSeriesChartDataPoint[]): boolean =>
    data.every(
      (item) => item.mean === null && item.low === null && item.high === null
    );

  // Function to toggle visibility of a line //
  const toggleVisibility = (dataKey: string) => {
    setVisibility((v) => ({ ...v, [dataKey]: !v[dataKey] }));
  };
  
  // Configuration for lines in the chart //
  const lines: LinePayload[] = [
    { dataKey: "mean", color: "#8884d8", value: "Mean" },
    { dataKey: "low", color: "#82ca9d", value: "Low" },
    { dataKey: "high", color: "#ff7300", value: "High" },
  ];

  return (
    <div className="flex flex-col flex-auto">
      {isDataEmpty(data) && (
        <div className="fixed inset-0 justify-center items-center flex pointer-events-none">
          <div className="text-2xl max-w-lg text-orange-600 w-full text-center">
            No data available for the selected routes.
          </div>
        </div>
      )}
      <ResponsiveContainer
        width={width ?? "100%"}
        height={height ?? "100%"}
        minHeight={320}
      >
        <AreaChart
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          
          {/* Configuration of the chart, including grid, axes, and tooltip */}
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="day" />
          <YAxis />
          <Tooltip />
          {lines.map(
            (line) =>
              visibility[line.dataKey] && (
                <Area
                  key={line.dataKey}
                  type="monotone"
                  dataKey={line.dataKey}
                  stackId="1"
                  stroke={line.color}
                  fill={line.color}
                />
              )
          )}
        </AreaChart>
      </ResponsiveContainer>

      {/* Toggle controls for the lines */}
      <ToggleLines
        payload={lines}
        visibility={visibility}
        toggleVisibility={toggleVisibility}
      />
    </div>
  );
};

export default TimeSeriesChart;
