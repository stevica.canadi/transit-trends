import esbuild from 'esbuild';
import {nodeExternalsPlugin} from 'esbuild-node-externals'

esbuild
  .build({
    entryPoints: ["./src/index.ts"],
    outdir: "dist",
    bundle: true,
    sourcemap: false,
    minify: true,
    splitting: false,
    treeShaking: true,
    platform: 'neutral',
    format: 'esm',
    target: 'es2015',
    plugins: [nodeExternalsPlugin()],
  })
  .catch(() => process.exit(1));