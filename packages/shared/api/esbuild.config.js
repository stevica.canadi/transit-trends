// Using ES Module import statements
import esbuild from "esbuild";
import { nodeExternalsPlugin } from "esbuild-node-externals";

esbuild
  .build({
    entryPoints: ["src/index.server.ts"],
    outfile: "./dist/index.server.js", 
    bundle: true,
    sourcemap: false,
    minify: true,
    splitting: false,
    treeShaking: true,
    platform: 'node',
    format: 'esm',
    target: 'es2015',
    plugins: [nodeExternalsPlugin()],
  })
  .catch(() => process.exit(1));
