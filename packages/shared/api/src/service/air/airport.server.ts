
/**
 * Fetches a list of airports.
 * @returns Promise<Airport[]>
 * @throws Will throw an error if the fetching fails
 */

import { fetchWithToken } from "../token.server";


export type Airport = {
  code: string;
  name: string;
};

export async function fetchAirports(): Promise<Airport[]> {
    try {
      const response = await fetchWithToken("air/airports");
      const data = await response.json();
      return data as Airport[];
    } catch (error) {
      if (error instanceof Error) {
        console.error("Fetching error Airports:", error.message);
      } else {
        console.error("Unknown error occurred during fetching Airports");
      }
      throw error;
    }
}
