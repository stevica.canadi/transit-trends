
/**
 * Fetches a list of rates for Airports.
 * @returns Promise<Rate[]>
 * @throws Will throw an error if the fetching fails or if required parameters are missing
 */

export type Rate = {
  day: string;
  mean: number;
  low: number;
  hight: number;
}


import { fetchWithToken } from "../token.server";

export async function fetchAirportRates({
  request,
}: {
  request: Request;
}): Promise<Rate[]> {
  const url = new URL(request.url);
  const searchParams = new URLSearchParams(url.search);
  const origin = searchParams.get("origin");
  const destination = searchParams.get("destination");

  if (!origin || !destination) {
    throw new Error("Origin and destination parameters are required");
  }

  try {
    const response = await fetchWithToken(
      `air/rates?origin=${origin}&destination=${destination}`
    );
    const data = await response.json();
    return data as Rate[];
  } catch (error) {
    if (error instanceof Error) {
      console.error("Fetching error rates for Airport Rates:", error.message);
    } else {
      console.error(
        "Unknown error occurred during fetching rates for Airport Rates"
      );
    }
    throw error;
  }
}
