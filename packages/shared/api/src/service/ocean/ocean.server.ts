
/**
 * Fetches a list of Ports.
 * @returns Promise<Ports[]>
 * @throws Will throw an error if the fetching fails
 */

import { fetchWithToken } from "../token.server";

export type Port = {
  code: string;
  name: string;
};

export async function fetchPorts(): Promise<Port[]> {
    try {
      const response = await fetchWithToken("ocean/ports");
      const data = await response.json();
      return data as Port[];
    } catch (error) {
      if (error instanceof Error) {
        console.error("Fetching error Ports:", error.message);
      } else {
        console.error("Unknown error occurred during fetching Ports");
      }
      throw error;
    }
}
