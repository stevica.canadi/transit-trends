/**
 * Performs an API request to the specified endpoint using an API token for authorization.
 * @param {string} endpoint - The API endpoint to fetch.
 * @returns {Promise<Response>} - The response from the fetch request.
 * @throws {Error} - Throws an error if the API token is not set or if the fetch request fails.
 */

export async function fetchWithToken(endpoint: string): Promise<Response> {
  const API_URL = process.env.API_URL;
  const TOKEN = process.env.TOKEN;
  
  if (!TOKEN) {
    throw new Error("API token is not set");
  }

  const response = await fetch(API_URL + endpoint, {
    headers: {
      "x-api-key": TOKEN,
    },
  });

  if (!response.ok) {
    throw new Error(`Error: ${response.status}`);
  }

  return response;
}