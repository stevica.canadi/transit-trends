import {fetchWithToken} from './service/token.server'
import {fetchAirports} from './service/air/airport.server'
import {fetchAirportRates} from './service/air/rates.server'
import {fetchPorts} from './service/ocean/ocean.server'
import {fetchPortRates} from './service/ocean/rates.server'

export {
    fetchWithToken,
    fetchAirports,
    fetchAirportRates,
    fetchPortRates,
    fetchPorts
}