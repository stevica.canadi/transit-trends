import { json, LoaderFunctionArgs, type MetaFunction } from "@remix-run/node";
import { useLoaderData, useSearchParams } from "@remix-run/react";
import { fetchPorts, fetchPortRates } from "@xeneta/api";
import { Port } from "@xeneta/api/dist/types/service/ocean/ocean.server";
import { Alert, ComboBox, TimeSeriesChart } from "@xeneta/ui-library";

export const meta: MetaFunction = () => {
  return [
    { title: "Transit Trends / Ocean Port Rates" },
    { name: "description", content: "" },
  ];
};

export const loader = async ({ request }: LoaderFunctionArgs) => {
  // Fetch list of ports.

  const ports = await fetchPorts();
  const url = new URL(request.url);
  const searchParams = new URLSearchParams(url.search);
  let origin = searchParams.get("origin");
  let destination = searchParams.get("destination");

  // Ensure origin and destination are not the same.
  if (origin === destination) {
    destination = null;
  }

  let rates;

  // Fetch rates if both origin and destination are provided.
  if (origin && destination) {
    rates = await fetchPortRates({ request });
  }

  return json({ ports, rates });
};

export default function Index() {
  // Use the data loaded by the loader.
  const data = useLoaderData<typeof loader>();

  const [params, setParams] = useSearchParams();
  
  // Extract 'origin' and 'destination' from the URL parameters.
  const origin = params.get("origin");
  const destination = params.get("destination");

  // Check if the location (origin and destination) is the same.
  const locationIsTheSame = origin && destination && origin === destination;

  // Function to update query parameters.
  const setQueryParams = (value: string | undefined, param: string) => {
    if (value) {
      params.set(param, value);
    } else {
      params.delete(param);
    }
    setParams(params);
  };

  // Function to set the destination parameter.
  const setDestinationParams = (value: string | undefined, param: string) => {
    if (value) {
      params.set(param, value);
    }
    setParams(params);
  };
  
  // Map ports data to options for the ComboBox component.
  const portOptions = data.ports.map((port: Port) => {
    return { value: port.code, label: `${port.name} (${port.code})` };
  });

  return (
    <div className="font-sans flex flex-col h-screen flex-auto pr-12">
      <div className="w-full px-8 pt-12 pb-8 mb-4 flex gap-6 pl-20 shrink-0">
        
        {/* ComboBox for selecting the origin port. */}
        <ComboBox
          label="Origin"
          value={origin ?? undefined}
          onChange={(option) =>
            option?.value && setDestinationParams(`${option?.value}`, "origin")
          }
          options={portOptions}
          id="origin"
          placeholder="Select origin"
          className="w-64"
        />

        {/* ComboBox for selecting the destination port. Disabled if no origin is selected. */}
        <ComboBox
          isDisabled={!origin}
          label="Destination"
          value={destination ?? undefined}
          onChange={(option) =>
            option?.value && setQueryParams(`${option?.value}`, "destination")
          }
          options={portOptions}
          id="destination"
          placeholder="Select destination"
          className="w-64"
        />
      </div>
      
      {/* Conditionally render the TimeSeriesChart or an Alert based on the availability of rates data. */}
      {data.rates ? (
        <TimeSeriesChart data={data.rates} />
      ) : (
        <Alert
          title="No Origin or Destination"
          message={`Please select an origin and a destination ${
            locationIsTheSame ? "that are different" : ""
          }.`}
        />
      )}
    </div>
  );
}
