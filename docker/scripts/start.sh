#!/bin/sh

# Start air-freight app in the background
echo "Starting air-freight app..."
cd packages/air-freight && PORT=4001 npm run start &

# Start ocean-freight app in the foreground
echo "Starting ocean-freight app..."
cd packages/ocean-freight && PORT=4002 npm run start