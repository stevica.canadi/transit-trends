# Docker

Review [docker-compose.yaml](docker-compose.yaml) to see specifications like ports and available services.

## Helpful commands

_Commands must be executed from this folder._

To spin up the whole system: `cd docker/docker compose up`

To force rebuild of the images: `cd docker/docker compose up -d --build`

